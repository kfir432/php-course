<?php
/**
 * Created by PhpStorm.
 * User: kfirevronserver
 * Date: 30/01/2018
 * Time: 9:36
 */

class Controller
{
    //Load model
    public function model($model)
    {
        //require model file
        require_once '../app/models/' . $model . '.php';

        return new $model();
    }

    //Load view
    public function view($view, $data = [])
    {
        if(file_exists('../app/views/'.$view.'.php')){
            require_once '../app/views/'.$view.'.php';
        }else{
            die('View does not exist');
        }
    }
}