<?php
/**
 * Created by PhpStorm.
 * User: kfirevronserver
 * Date: 30/01/2018
 * Time: 9:35
 * App Core Class
 * creates url & loads core controller
 *
 */

class Core{
    protected $currentController = 'Pages';
    protected $currentMethod = 'index';
    protected $params = [];

    public function __construct()
    {
        $url = $this->getUrl();

        if(file_exists('../app/controllers/'.ucwords($url[0].'.php'))){
            $this->currentController = ucwords($url[0]);
            unset($url[0]);
        }

        require_once '../app/controllers/'.$this->currentController.'.php';
        $this->currentController = new $this->currentController;
        if(isset($url[1])){
            var_dump($url);
            //check to see if method exists
            if(method_exists($this->currentController,$url[1])){
                $this->currentMethod = $url[1];
                unset($url[1]);
            }
        }
       // get params

        $this->params = $url ? array_values($url) : [];
        var_dump($this->currentMethod);

        // Call a callback with array of params
        call_user_func_array([$this->currentController,$this->currentMethod],$this->params);
    }

    public function getUrl(){
//        $extensions = array("php", "jpg", "jpeg", "gif", "css");
//
        $path = parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH);
//        $ext = pathinfo($path, PATHINFO_EXTENSION);
//        if (in_array($ext, $extensions)) {
//            // let the server handle the request as-is
//            return false;
//        }
        if(isset($path)){
            $url = rtrim($path,'/');
            $url = filter_var($url,FILTER_SANITIZE_URL);
            $url = explode('/',$url);
            //array_splice($url, 0, 1);
            return $url;
        }
    }
}

