<?php
/**
 * Created by PhpStorm.
 * User: kfirevronserver
 * Date: 30/01/2018
 * Time: 9:38
 */
//Load config file;
require_once 'config/config.php';

//Load libraries

//require_once 'libraries/Core.php';
//require_once 'libraries/Controller.php';
//require_once 'libraries/Database.php';

spl_autoload_register(function ($className){
   require_once 'libraries/'.$className.'.php';
});