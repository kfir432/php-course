<?php
/**
 * Created by PhpStorm.
 * User: kfirevron
 * Date: 29/01/2018
 * Time: 22:24
 */

class User
{
    private $name;
    private $age;

    public function __construct($name, $age)
    {
        $this->name = $name;
        $this->age = $age;
    }

    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }
    }

    public function __set($property,$value)
    {
        if(property_exists($this,$property)){
            $this->$property = $value;
        }
    }
}

$user = new User('kfir', 25);

echo $user->__set('age',32);
echo $user->__get('age');