<?php
/**
 * Created by PhpStorm.
 * User: kfirevron
 * Date: 29/01/2018
 * Time: 22:24
 */

class User
{
    protected $name = 'kfir';
    protected $age;

    public function __construct($name, $age)
    {
        $this->name = $name;
        $this->age = $age;
    }
}

class Customer extends User
{
    private $balance;

    public function __construct($name,$age,$balance)
    {
        parent::__construct($name,$age);
        $this->balance = $balance;
    }

    public function pay($amount)
    {
        return $this->name . ' paid $' . $amount;
    }
}

$customer = new Customer('kfir', 32,200);

echo $customer->pay(25);