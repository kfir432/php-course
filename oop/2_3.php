<?php
/**
 * Created by PhpStorm.
 * User: kfirevron
 * Date: 29/01/2018
 * Time: 22:24
 */

class User
{
    public $name;
    public $age;

    public function __construct($name,$age)
    {
        $this->name = $name;
        $this->age = $age;
    }

    public function sayHello()
    {
        return $this->name . ' says hello';
    }
}

$user = new User('kfir',25);

echo $user->sayHello();