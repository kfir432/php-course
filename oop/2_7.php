<?php
/**
 * Created by PhpStorm.
 * User: kfirevron
 * Date: 29/01/2018
 * Time: 22:24
 */

class User
{
    public $name;
    public $age;
    public static $minPassLength = 6;

    public static function validatePass($pass){
        if(strlen($pass) >= self::$minPassLength){
            return true;
        }else{
            return false;
        }
    }
}

$password = '123456';

if(User::validatePass($password)){
    echo 'password valid';
}else{
    echo 'password not valid';
}